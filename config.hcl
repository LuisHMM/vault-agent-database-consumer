pid_file = "./pidfile"

vault {
        address = "http://127.0.0.1:8200"
         tls_disable = true
}


auto_auth {
        method "approle" {
                config = {
			role_id_file_path = "/tmp/role_id.txt"
			secret_id_file_path  = "/tmp/secret_id.txt"
                }
        }

        sink "file" {
                config = {
                        path = "/tmp/agent-token"
                }
        }
}

cache {
        use_auto_auth_token = true
}

template {
  source      = "template.ctmpl"
  destination      = "creds.txt"
}

listener "tcp" {
         address = "127.0.0.1:8100"
         tls_disable = true
}


