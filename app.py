import pymongo
import random
import time


def getClient():
    with open('creds.txt') as f:
        data = f.read()

    data = data[:-1]
    data = data.split(' ')

    username = data[0]
    password = data[1]
    return pymongo.MongoClient('mongodb://{}:{}@{}/?tls=false'.format(
        username,
        password,
        address)
    )


address = "172.17.0.2:27017"


def writeData():
    client = getClient()

    db = client['admin']
    collections = [str(random.randint(100,999)) for _ in range(10)]

    for col in collections:
        db[col].insert_one({'test':True})
    return collections


collections = writeData()

while True:
    client = getClient()
    db = client['admin']
    for col in collections:
        data = db[col].find_one()
        print(data)
        assert 'test' in data and data['test'] == True
    time.sleep(2)

